<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\People>
 */
class PeopleFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name(),
            'age'=>$this->faker->numberBetween($min = 1, $max = 100),
            'bio'=>$this->faker->realText($maxNbChars = 200, $indexSize = 2),

           
        ];
    }
}